package mst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Clase que permite obtener un Árbol cobertor mínimo a partir de un grafo
 * @author Felipe
 */
public class MST {
    
    public int nodos;
    public int aristas;
    public ArrayList<Arista> aristasTodas;
    public ArrayList<Arista> [] aristasPorNodo;
    public boolean [] nodosRecorridos;
    public ArrayList<Arista> MST;
    
    /**
     * Encuentra el árbol cobertor mínimo (MST) del grafo leído
     */
    public void encontrarMST(){
        ArrayList<Arista> siguientes = this.aristasPorNodo[0];
        // Ordenar la lista de arista(s) siguiente(s)
        
        // Agregar la primera arista al MST, marcando los nodos recorridos
        Arista a = siguientes.get(0);
        
        // -END- Agregar la primera arista al MST, marcando los nodos recorridos
        
        boolean fin;
        while (true)
        {
            siguientes = new ArrayList();
            fin = true;
            for (int v = 0; v < nodos; v++){
                // Si el nodo v no ha sido recorrido
                    // fin = false
                //else:
                    // Agregar al arreglo siguientes las aristas del nodo
            }
            if (fin) return;
            
            // Ordenar la lista de aristas siguientes
            for (int i = 0; i < siguientes.size(); i++){
                a = siguientes.get(i);
                
                // Si los nodos que une la arista no han sido recorridos
                    // Marcar los nodos como recorridos
                    // Agregar la arista al MST
                    // break
            }
        }
    }
    
    /**
     * Entrega por salida estándar la suma de pesos del MST
     */
    public void printPesosMST(){
        // Inicializa valor
        // Suma pesos en un ciclo
        // Imprimir valor
    }
    
    /**
     * Entrega por salida estándar las aristas que forman el MST.
     */
    public void printMST(){
        // si MST está vacío (probablemente no se ha buscado el MST)
            // Encontrar MST
        
        // Ordenar las aristas del MST
        //Imprimir los valores
    }
    
    /**
     * Entrega por salida estándar todas las aristas que conforman el grafo.
     */
    public void printAristasGrafo(){
        for (int v = 0; v < nodos; v++){
            System.out.println("" + v + " -> " + aristasPorNodo[v]);
        }
    }
    
    /**
     * Lee el archivo indicado y guarda los datos en la estructura
     * @param nombreArchivo nombre del archivo que contiene los datos
     */
    public void leerArchivo(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File(nombreArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // ---- Lectura del fichero ----
            String linea;
            linea = br.readLine();
            String [] datos = linea.split(" ");
            // Inicializar atributos y arreglos
            
            int a = 0;
            int origen, destino;
            int peso;
            while ((linea = br.readLine()) != null && a<aristas) {
                // Obtener datos a partir de una línea
                // Asignar los datos a las variables origen, destino, peso
                // Crear las aristas y agregarlas a los arraylists
                a++;
            }
            // --- FIN Lectura del fichero ---
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
