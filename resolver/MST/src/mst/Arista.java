package mst;

/**
 * Clase que representa una arista.
 * Incluye el identificador de ambos nodos y el peso de la arista
 * @author Felipe
 */
public class Arista implements Comparable<Arista>{
    
    public int origen;
    public int destino;
    public int peso;

    public Arista(int origen, int destino, int peso) {
        this.origen = origen;
        this.destino = destino;
        this.peso = peso;
    }

    /**
     * Entrega por salida estándar los valores de la arista
     */
    public void printValores() {
        System.out.println("" + origen + " " + destino + " " + peso);
    }
    
    @Override
    public String toString() {
        return "Arista{" + "origen=" + origen + ", destino=" + destino + ", peso=" + peso + '}';
    }

    /**
     * Compara una arista a otra en base al peso
     * @param other Objeto con el cual se desea comparar
     * @return valor mayor a 0 si el peso de "o" es mayor, menor a 0 si el peso de "o" es menor, 0 en otro caso.
     */
    @Override
    public int compareTo(Arista other) {
        return Double.compare(this.peso, other.peso);
    }
}
