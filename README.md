# Árbol cobertor mínimo (MST)
En este ejercicio se propone encontrar un árbol cobertor mínimo para un grafo no direccionado dado.
Se proveen archivos con casos de prueba y sus respectivas soluciones.

## Archivos de prueba
La estructura de los archivos consiste en una primera línea indicando la cantidad de nodos **N** del grafo y la cantidad de aristas **A**, seguido de **A** líneas con 3 números indicando los nodos de origen, destino y peso de cada arista.
Los números de cada línea vienen separados por un espacio.  
La solución correspondiente son los datos de cada arista que conforma el MST en una línea diferente, separados por un espacio, en el mismo orden que fueron ingresados (origen destino peso), seguido por una última línea con la suma de los pesos de todas estas aristas.

### Ejemplo
Archivo de prueba:

    3 2
    0 1 3
    0 2 3
    2 1 4

Solución:

    0 1 3
    0 2 3
    6

## Consideraciones
El grafo ingresado es no dirigido.  
Solo debe funcionar para ejemplos correctos.  
Los nodos se enumeran desde 0 hasta N-1.