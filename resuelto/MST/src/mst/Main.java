package mst;

/**
 * Clase wrapper
 * @author Felipe
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            return;
        }
        MST mst = new MST();
        mst.leerArchivo(args[0]);
        mst.printAristasGrafo();
        mst.encontrarMST();
        mst.printMST();
        mst.printPesosMST();
    }
}
