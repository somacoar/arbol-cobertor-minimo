package mst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Clase que permite obtener un Árbol cobertor mínimo a partir de un grafo
 * @author Felipe
 */
public class MST {
    
    public int nodos;
    public int aristas;
    public ArrayList<Arista> aristasTodas;
    public ArrayList<Arista> [] aristasPorNodo;
    public boolean [] nodosRecorridos;
    public ArrayList<Arista> mst;
    
    
    /**
     * Entrega por salida estándar las aristas que forman el MST.
     */
    public void printMST(){
        for(Arista arista: mst){
            arista.printValores();
        }
    }
    
    /**
     * Entrega por salida estándar todas las aristas que conforman el grafo.
     */
    public void printAristasGrafo(){
        for (int v = 0; v < nodos; v++){
            System.out.println("" + v + " -> " + aristasPorNodo[v]);
        }
    }
    
    /**
     * Entrega por salida estándar la suma de pesos del MST
     */
    public void printPesosMST(){
        int peso = 0;
        for(Arista arista:mst){
            peso += arista.peso;
        }
        System.out.println(peso);
    }
     
    /**
     * Encuentra el árbol cobertor mínimo (MST) del grafo leído
     */
    public void encontrarMST(){
        ArrayList<Arista> adyacentes = this.aristasPorNodo[0];  //parto del nodo 0
        Collections.sort(adyacentes);
        Arista a = adyacentes.get(0);
        this.nodosRecorridos[a.origen] = true;
        this.nodosRecorridos[a.destino] = true;
        this.mst.add(a);
        
        boolean ciclo = true;
        while(ciclo){
            adyacentes = new ArrayList();
            for(int n = 0; n < nodos; n++){
                if(nodosRecorridos[n]){
                    adyacentes.addAll(this.aristasPorNodo[n]);
                }
            }
            Collections.sort(adyacentes);
            
            for(int i = 0; i < adyacentes.size(); i++){
                a = adyacentes.get(i);
                if(!(nodosRecorridos[a.origen] && nodosRecorridos[a.destino])){
                    this.mst.add(a);
                    this.nodosRecorridos[a.origen] = true;
                    this.nodosRecorridos[a.destino] = true;
                    break;
                }
            }
            ciclo = false;
            for(int i = 0; i < nodosRecorridos.length ; i++){
                if (!nodosRecorridos[i]){
                     ciclo = true;
                     break;
                }
            }
        }
    }
    
    
    /**
     * Lee el archivo indicado y guarda los datos en la estructura
     * @param nombreArchivo nombre del archivo que contiene los datos
     */
    public void leerArchivo(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File(nombreArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // ---- Lectura del fichero ----
            String linea;
            linea = br.readLine();
            String [] datos;
            datos = linea.split(" ");   //elimina el espacio
            this.nodos = Integer.parseInt(datos[0]);
            this.aristas = Integer.parseInt(datos[1]);
            this.aristasPorNodo = new ArrayList[nodos];
            this.nodosRecorridos = new boolean[nodos];
            this.mst = new ArrayList();
            this.aristasTodas = new ArrayList(aristas);
            
            for(int n=0; n<nodos; n++){
                this.aristasPorNodo[n] = new ArrayList();
            }
            
            int a = 0;
            int origen, destino, peso;
            while((linea = br.readLine()) != null && a < aristas){
                datos = linea.split(" ");
                origen = Integer.parseInt(datos[0]);
                destino = Integer.parseInt(datos[1]);
                peso = Integer.parseInt(datos[2]);
                Arista arista = new Arista(origen, destino, peso);
                this.aristasTodas.add(arista);                
                this.aristasPorNodo[origen].add(arista);
                this.aristasPorNodo[destino].add(arista);
                a++;
            }
            //-------------------------------------------
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
